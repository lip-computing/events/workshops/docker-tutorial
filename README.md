# Docker Tutorial

Clone this repository:

``` bash
git clone https://gitlab.com/lip-computing/events/workshops/docker-tutorial.git
```

## [Install Docker Engine on Fedora](https://docs.docker.com/engine/install/fedora/)

Run the following script: `cd docker-tutorial && ./bin/install_fedora.run`

## [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

Run the following script: `cd docker-tutorial && ./bin/install_ubuntu.run`

## [Install Podman](https://podman.io/docs/installation)

### Fedora

Run the script: `bin/podman_fedora.run`

### Ubuntu

Run the script: `bin/podman_ubuntu.run`

## [Install Docker Desktop on Windows](https://docs.docker.com/desktop/install/windows-install/)

After downloading Docker Desktop Installer.exe, run the following command in a terminal to install Docker Desktop:

 `"Docker Desktop Installer.exe" install`

If you’re using PowerShell you should run it as:

``` powershell
Start-Process 'Docker Desktop Installer.exe' -Wait install
```

If using the Windows Command Prompt:

``` bash
start /w "" "Docker Desktop Installer.exe" install
```

By default, Docker Desktop is installed at `C:\Program Files\Docker\Docker`.

## [Install Podman Desktop](https://podman-desktop.io/downloads)

To install Podman Desktop:

1- [Download the Windows installer](https://podman-desktop.io/downloads/windows).

2- To start the Podman Desktop installer, open the downloaded file.

![alt text](imgs/podman_desktop.png)

## Let's get started

You can clone the following sample application or you can use your own use case.

``` bash
git clone https://github.com/docker/getting-started-app.git
```

### Create Dockerfile and build the docker image

Make sure you're in the getting-started-app directory. Replace /path/to/getting-started-app with the path to your getting-started-app directory.

```bash
cd /path/to/getting-started-app
```

1. Create an empty file named `Dockerfile`.

```bash
touch Dockerfile
```

2. Using a text editor or code editor, add the following contents to the Dockerfile:

``` Dockerfile
# syntax=docker/dockerfile:1

FROM node:18-alpine
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
EXPOSE 3000
```

If you are using your own example, change the `WORKDIR`, `COPY`, `RUN`, `CMD` and `EXPOSE` accordingly. `RUN` should be optimized to minimize the required layers for the produced image.

3. Build the image

`docker build` command creates the image with the desired [custom image name](https://docs.docker.com/reference/cli/docker/image/tag/#description) `[HOST[:PORT_NUMBER]/]PATH[:TAG]` when adding the flag `-t`. If omitted the last part (`:v1.0.0`), `:latest` tag will be assumed.

```bash
docker build -t getting-started:1.0.0 .
```

[Additional image names](https://docs.docker.com/reference/cli/docker/image/tag/) can be created later referencing existing ones with the command

```bash
docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

### Start an app container

Now that you have an image, you can run the application in a container using the `docker run` command.

1. Run your container and specify the name of the image you just created:

```bash
docker run -dp 127.0.0.1:3000:3000 getting-started:1.0.0
```

The `-d` flag (short for `--detach`) runs the container in the background. This means that Docker starts your container and returns you to the terminal prompt. You can verify that a container is running by viewing it in Docker Dashboard under Containers, or by running `docker ps` in the terminal.

The `-p` flag (short for `--publish`) creates a port mapping between the host and the container. The `-p` flag takes a string value in the format of `HOST:CONTAINER`, where `HOST` is the address on the host, and `CONTAINER` is the port on the container. The command publishes the container's port 3000 to `127.0.0.1:3000 (localhost:3000)` on the host. Without the port mapping, you wouldn't be able to access the application from the host.

2. After a few seconds, if using the sample app, open your web browser to [http://localhost:3000](http://localhost:3000). You should see your app. Add an item or two and see that it works as you expect.

![alt text](imgs/sample_app_screenshot.png)

## Common tasks when using Docker

### Access docker container

After launching a docker container you can access to a command line shell with two possible approaches:

- `docker run -i` allows to launch an interactive session after starting the container (flag `-d` must be omitted from previous example)
- `docker exec -it CONTAINER COMMAND [ARG...]` to access a container running on the background with a command with args

### Launch a container and remove it when stopped

Sometimes we only need to check some image or run an application like a job. To avoid keeping the container storage space after it exits, you can execute `docker run` with `--rm` flag.

Example testing latest Fedora image:

```bash
docker run --rm -it fedora bash
```

This example shows the immutable property of Docker images and the mutable runtime context provided by Docker container.

### Check image layer details

Sometimes we want to check the image registered commands when it was built. For that you can use the command `docker history [OPTIONS] IMAGE`. Example:

```bash
docker image history getting-started
```

You should get output that looks something like the following.

```bash
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
a78a40cbf866        18 seconds ago      /bin/sh -c #(nop)  CMD ["node" "src/index.jâ¦    0B                  
f1d1808565d6        19 seconds ago      /bin/sh -c yarn install --production            85.4MB              
a2c054d14948        36 seconds ago      /bin/sh -c #(nop) COPY dir:5dc710ad87c789593â¦   198kB               
9577ae713121        37 seconds ago      /bin/sh -c #(nop) WORKDIR /app                  0B                  
b95baba1cfdb        13 days ago         /bin/sh -c #(nop)  CMD ["node"]                 0B                  
<missing>           13 days ago         /bin/sh -c #(nop)  ENTRYPOINT ["docker-entryâ¦   0B                  
<missing>           13 days ago         /bin/sh -c #(nop) COPY file:238737301d473041â¦   116B                
<missing>           13 days ago         /bin/sh -c apk add --no-cache --virtual .buiâ¦   5.35MB              
<missing>           13 days ago         /bin/sh -c #(nop)  ENV YARN_VERSION=1.21.1      0B                  
<missing>           13 days ago         /bin/sh -c addgroup -g 1000 node     && adduâ¦   74.3MB              
<missing>           13 days ago         /bin/sh -c #(nop)  ENV NODE_VERSION=12.14.1     0B                  
<missing>           13 days ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B                  
<missing>           13 days ago         /bin/sh -c #(nop) ADD file:e69d441d729412d24â¦   5.59MB   
```

You'll notice that several of the lines are truncated. If you add the --no-trunc flag, you'll get the full output.

```bash
docker image history --no-trunc getting-started
```

You can also check the image configuration details looking into the result of `docker image inspect [OPTIONS] IMAGE`.

### Update the application

1. Get the ID of the container by using the `docker ps` command.

2. Remove running container replacing `CONTAINER_ID` by the container name or id.

```bash
docker stop CONTAINER_ID
docker rm CONTAINER_ID
```

3. If using the sample app, in the `src/static/js/app.js` file, update line 56 to use the new empty text.

```diff
- <p className="text-center">No items yet! Add one above!</p>
+ <p className="text-center">You have no todo items yet! Add one above!</p>
```

4. Build your updated version of the image with the build command.

```bash
docker build -t getting-started:1.0.1 .
```

This will create a new image that will be associated with the release 1.0.1 (associated with the patch).

It is also possible to replace previous image. Just fill after `-t` the same image name.

5. Start again the container

```bash
docker run -dp 127.0.0.1:3000:3000 getting-started:1.0.1
```

### Publish a docker image to a registry

To push an image, you first need to create a repository on Docker Hub.

1. [Sign up](https://www.docker.com/pricing?utm_source=docker&utm_medium=webreferral&utm_campaign=docs_driven_upgrade) or [Sign in](https://hub.docker.com/) to Docker Hub.
2. Select the Create Repository button.
3. For the repository name, use `getting-started` or the name you would like for your project. Make sure the **Visibility is Public**.
4. Select **Create**.

In the following image, you can see an example Docker command from Docker Hub. This command will push to this repository.

![alt text](imgs/dockerhub_push.png)

5. In the command line, run the `docker push` command that you see on Docker Hub. Note that your command will have your Docker ID, not "docker". For example, `docker push YOUR-USER-NAME/getting-started`.

```bash
$ docker tag getting-started:1.0.0 YOUR-USER-NAME/getting-started:1.0.0
$ docker push YOUR-USER-NAME/getting-started:1.0.0
The push refers to repository [docker.io/YOUR-USER-NAME/getting-started]
An image does not exist locally with the tag: YOUR-USER-NAME/getting-started
```

It is required first to add a new tag for the local image, so it will be in accordance with the namespace of the remote registry.

6. To get the image from the registry you only need to run `docker pull` command. For example:

```bash
docker pull YOUR-USER-NAME/getting-started:1.0.0
```

7. You can also remove local images to avoid mistake when following remote images.

```bash
docker image rm getting-started:1.0.0
```

### Persist your data

You can use [container volumes](https://docs.docker.com/get-started/05_persisting_data/) or [bind mounts](https://docs.docker.com/get-started/06_bind_mounts/) to keep data after removing a container. This is important when there are contents being stored by the application.

When a container runs, it uses the various layers from an image for its filesystem. Each container also gets its own "scratch space" to create/update/remove files. Any changes won't be seen in another container, even if they're using the same image.

Volumes provide the ability to connect specific filesystem paths of the container back to the host machine. If you mount a directory in the container, changes in that directory are also seen on the host machine. If you mount that same directory across container restarts, you'd see the same files.

A bind mount is another type of mount, which lets you share a directory from the host's filesystem into the container. When working on an application, you can use a bind mount to mount source code into the container. The container sees the changes you make to the code immediately, as soon as you save a file. This means that you can run processes in the container that watch for filesystem changes and respond to them.

### Docker container logs

To check the application runtime logs when running the container in the background, you can run the command `docker logs [OPTIONS] CONTAINER`.
The `docker logs` command batch-retrieves logs present at the time of execution.
The `docker logs --follow` command will continue streaming the new output from the container's `STDOUT` and `STDERR`.
The `docker logs --details` command will add on extra attributes, such as environment variables and labels, provided to --log-opt when creating the container.

You can check more details about `docker logs` in the [Docker reference documentation](https://docs.docker.com/reference/cli/docker/container/logs/).

There are many supported [logging drivers](https://docs.docker.com/config/containers/logging/configure/#supported-logging-drivers) for multiple platforms.

### Define an entrypoint

The ENTRYPOINT instruction works very similarly to CMD in that it is used to specify the command executed when the container is started. However, where it differs is that ENTRYPOINT doesn't allow you to override the command.

With the ENTRYPOINT instruction, it is not possible to override the instruction during the docker run command execution like it can with CMD. This highlights another usage of ENTRYPOINT, as a method of ensuring that a specific command is executed when the container in question is started regardless of attempts to override the ENTRYPOINT.

Inside the Dockerfile, after the first steps of preparing the image, you can add the ENTRYPOINT definition as follows:

```bash
ENTRYPOINT ["executable", "param1", "param2"]
```

For more details check [Docker reference documentation](https://docs.docker.com/reference/dockerfile/#entrypoint).

### Restart policy

The following command starts a Redis container and configures it to always restart, unless the container is explicitly stopped, or the daemon restarts.

```bash
docker run -d --restart unless-stopped redis
```

More details at [Docker reference documentation](https://docs.docker.com/config/containers/start-containers-automatically/).

## Advanced topics on development practices

Clone the getting started repository:

```bash
git clone https://github.com/docker/getting-started.git
```

Check Dockerfile, build.sh and docker-compose files.
Understand platforms and targets.

**Review:**

- [Docker compose build specification (buildkit)](https://docs.docker.com/compose/compose-file/build/)
- [Docker compose buildx build command](https://docs.docker.com/reference/cli/docker/buildx/build/)

### Image building best practicies

**Review:**

- [Layer caching](https://docs.docker.com/get-started/09_image_best/#layer-caching)
- [Multi-stage Builds](https://docs.docker.com/get-started/09_image_best/#multi-stage-builds)

### React example

When building React applications, you need a Node environment to compile the JS code (typically JSX), SASS stylesheets, and more into static HTML, JS, and CSS. If you aren't doing server-side rendering, you don't even need a Node environment for your production build. You can ship the static resources in a static nginx container.

```dockerfile
# syntax=docker/dockerfile:1
FROM node:18 AS build
WORKDIR /app
COPY package* yarn.lock ./
RUN yarn install
COPY public ./public
COPY src ./src
RUN yarn run build

FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
```

In the previous Dockerfile example, it uses the `node:18` image to perform the build (maximizing layer caching) and then copies the output into a nginx container.

### .dockerignore files

You can use a `.dockerignore` file to exclude files or directories from the build context.

```bash
# .dockerignore
node_modules
bar
```

This helps avoid sending unwanted files and directories to the builder, improving build speed, especially when using a remote builder.

When you run a build command, the build client looks for a file named `.dockerignore` in the root directory of the context. If this file exists, the files and directories that match patterns in the files are removed from the build context before it's sent to the builder.

If you use multiple Dockerfiles, you can use different ignore-files for each Dockerfile. You do so using a special naming convention for the ignore-files. Place your ignore-file in the same directory as the Dockerfile, and prefix the ignore-file with the name of the Dockerfile, as shown in the following example.

```bash
.
├── index.ts
├── src/
├── docker
│   ├── build.Dockerfile
│   ├── build.Dockerfile.dockerignore
│   ├── lint.Dockerfile
│   ├── lint.Dockerfile.dockerignore
│   ├── test.Dockerfile
│   └── test.Dockerfile.dockerignore
├── package.json
└── package-lock.json
```

A Dockerfile-specific ignore-file takes precedence over the `.dockerignore` file at the root of the build context if both exist.

The `.dockerignore` file is a newline-separated list of patterns similar to the file globs of Unix shells. For the purposes of matching, the root of the context is considered to be both the working and the root directory. For example, the patterns `/foo/bar` and `foo/bar` both exclude a file or directory named `bar` in the `foo` subdirectory of `PATH` or in the root of the Git repository located at `URL`. Neither excludes anything else.

If a line in `.dockerignore` file starts with `#` in column 1, then this line is considered as a comment and is ignored before interpreted by the CLI.

**Review:**

- [.dockerignore syntax](https://docs.docker.com/build/building/context/#syntax)


## Docker Compose

Docker Compose is a tool that helps you define and share multi-container applications. With Compose, you can create a YAML file to define the services and with a single command, you can spin everything up or tear it all down.

The big advantage of using Compose is you can define your application stack in a file, keep it at the root of your project repository (it's now version controlled), and easily enable someone else to contribute to your project. Someone would only need to clone your repository and start the app using Compose. In fact, you might see quite a few projects on GitHub/GitLab doing exactly this now.

### Create the Compose file

1. In the application directory create a file named `docker-compose.yaml`
2. Add the following content:

```yaml
services:
  app:
    image: node:18-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 127.0.0.1:3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos

  mysql:
    image: mysql:8.0
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```

### Run the application stack

1. Make sure no other copies of the containers are running first. Use `docker ps` to list the containers and `docker rm -f IDs` to remove them, where `IDs` are the container id from the related containers that must be deleted.

2. Start up the application stack using the `docker compose up` command. Add the `-d` flag to run everything in the background.

```bash
docker compose up -d
```

3. Look at the logs using the `docker compose logs -f` command. You'll see the logs from each of the services interleaved into a single stream. This is incredibly useful when you want to watch for timing-related issues. The `-f` flag follows the log, so will give you live output as it's generated. If you want to view the logs for a specific service, you can add the service name to the end of the logs command (for example, `docker compose logs -f app`).

### Tear it all down

When you're ready to tear it all down, simply run `docker compose down`. The containers will stop and the network will be removed.

By default, named volumes in your compose file are not removed when you run docker compose down. If you want to remove the volumes, you need to add the `--volumes` flag.

## Minikube and Kompose

### Kompose Installation

There is the support for several operating systems and also available packages for Linux distributions. You can check the [installation documentation](https://kompose.io/installation/) for the details. But for this tutorial we will use **Docker**, so no need to pre-install anything.

### Minikube Setup

Select the appropriate script for your Linux distribution:

- Fedora: `bin/minikube_fedora`
- Ubuntu: `bin/minikube_ubuntu`

To install `minikube` on other operating systems, check the [official documentation](https://minikube.sigs.k8s.io/docs/start/).

### Getting started

In this guide, we’ll deploy the `docker-compose.yaml` file to a minimal Kubernetes cluster launched locally on the PC using `minikube`.

1. Start minikube

```bash
$ minikube start
Starting local Kubernetes v1.7.5 cluster...
Starting VM...
Getting VM IP address...
Moving files into cluster...
Setting up certs...
Connecting to cluster...
Setting up kubeconfig...
Starting cluster components...
Kubectl is now configured to use the cluster
```

2. Use the Docker Compose file created for sample application or download an example Docker Compose file provided by Kompose.

```bash
wget https://raw.githubusercontent.com/kubernetes/kompose/main/examples/compose.yaml
```

3. Convert your Docker Compose file to Kubernetes receipts.

Run `kompose convert` in the same directory as your `docker-compose.yaml` file.

```bash
$ docker run --rm -it -v $PWD:/opt kompose sh -c "cd /opt && mkdir -p k8s && kompose convert -n my-namespace -o k8s"
INFO Kubernetes file "frontend-service.yaml" created
INFO Kubernetes file "redis-leader-service.yaml" created
INFO Kubernetes file "redis-replica-service.yaml" created
INFO Kubernetes file "frontend-deployment.yaml" created
INFO Kubernetes file "redis-leader-deployment.yaml" created
INFO Kubernetes file "redis-replica-deployment.yaml" created
```

Previous command will produce the kubernetes receipts to deploy the required services into the namespace "my-namespace" and place all generated files into the k8s directory.

4. Deploy the service in Kubernetes

```bash
minikube kubectl apply -f k8s/*.yaml
```

5. Access the service

```bash
minikube service app
```

or for the Kompose example

```bash
minikube service frontend
```

Check more details in the following reference pages:

- [kompose getting started](https://kompose.io/getting-started/)
- [minikube deploy applications](https://minikube.sigs.k8s.io/docs/start/)
